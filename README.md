# README #

### What is this repository for? ###
Possible implementation for using MMDrawerController as a split view controller in response to Stack Overflow question:

[http://stackoverflow.com/questions/24174864/mmdrawercontroller-keep-center-view-controller-completely-visible/24175591#24175591](http://stackoverflow.com/questions/24174864/mmdrawercontroller-keep-center-view-controller-completely-visible/24175591#24175591)