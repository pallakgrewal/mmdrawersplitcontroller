//
//  DrawerViewController.m
//  MMDrawerControllerTest
//
//  Created by Pallak Grewal on 2014-06-11.
//  Copyright (c) 2014 Pallak Grewal. All rights reserved.
//

#import "DrawerViewController.h"

@interface DrawerViewController ()

@end

@implementation DrawerViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    
    // Do sidebar stuff here
}

@end
