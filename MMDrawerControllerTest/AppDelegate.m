//
//  AppDelegate.m
//  MMDrawerControllerTest
//
//  Created by Pallak Grewal on 2014-06-11.
//  Copyright (c) 2014 Pallak Grewal. All rights reserved.
//

#import "AppDelegate.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import "DrawerViewController.h"
#import "CenterViewController.h"

#import "MMDrawerVisualState.h"

#import <QuartzCore/QuartzCore.h>

@interface AppDelegate ()
@property (nonatomic,strong) MMDrawerController *drawerController;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DrawerViewController *leftSideDrawerViewController = [[DrawerViewController alloc] init];
    
    // We need a dummy centre view controller to add to the drawer controller
    UIViewController *centerPlaceholderViewController = [[UIViewController alloc] init];
    
    // Actual view controller where content goes
    CenterViewController *centerViewController = [[CenterViewController alloc] init];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:centerViewController];
    // Adjust frame for nav controller so nav bar elements are centered
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.size.width = frame.size.width - 200.0f;
    navController.view.frame = frame;
    
    // Add nav controller as a child view controller to the dummy centre view controller
    [centerPlaceholderViewController addChildViewController:navController];
    [centerPlaceholderViewController.view addSubview:navController.view];

    
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:centerPlaceholderViewController
                             leftDrawerViewController:leftSideDrawerViewController];
    
    [self.drawerController setMaximumLeftDrawerWidth:200.0f];
    
    [self.drawerController openDrawerSide:MMDrawerSideLeft animated:NO completion:nil];
    // More drawer UI customization here.
    
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    // This is just for demo purposes.. to make sure your views are sized properly.
    // In your app you want the value to be MMCloseDrawerGestureModeNone
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setRootViewController:self.drawerController];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
