//
//  CenterViewController.m
//  MMDrawerControllerTest
//
//  Created by Pallak Grewal on 2014-06-11.
//  Copyright (c) 2014 Pallak Grewal. All rights reserved.
//

#import "CenterViewController.h"

@interface CenterViewController ()

@end

@implementation CenterViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Check that title is centered in nav bar. Since we resized
    // nav controller in app delegate, it is.
    self.title = @"Sleepy Kitty";
    
    // Pushing a new view controller
    UIBarButtonItem *pushViewBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Push VC" style:UIBarButtonItemStylePlain target:self action:@selector(pushNewViewController)];
    self.navigationItem.rightBarButtonItem = pushViewBarButtonItem;
    
    // Need to reset view size every time in view did load :(
    // Can make this frame a global app constant?
    self.view.backgroundColor = [UIColor yellowColor];
    CGRect frame = self.view.bounds;
    frame.origin.x = 200.0f;
    frame.size.width = frame.size.width - 200.0f;
    self.view.bounds = frame;
    
    NSLog(@"%@", self.view);
    UIImage *kitty = [UIImage imageNamed:@"1024.jpeg"];
    UIImageView *kittyImageView = [[UIImageView alloc] initWithImage:kitty];
    kittyImageView.frame = self.view.bounds;
    kittyImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:kittyImageView];
    
}

-(void)pushNewViewController
{
    CenterViewController *newCentreViewController = [[CenterViewController alloc] init];
    [self.navigationController pushViewController:newCentreViewController animated:YES];
}

@end
