//
//  main.m
//  MMDrawerControllerTest
//
//  Created by Pallak Grewal on 2014-06-11.
//  Copyright (c) 2014 Pallak Grewal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
